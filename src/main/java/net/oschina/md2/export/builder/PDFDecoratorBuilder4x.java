package net.oschina.md2.export.builder;

import com.lowagie.text.Document;
import com.lowagie.text.PageSize;

import net.oschina.md2.export.Decorator;
import net.oschina.md2.export.PDFDecorator4x;

public class PDFDecoratorBuilder4x implements DecoratorBuilder{

	public Decorator build() {
		return new PDFDecorator4x(new Document(PageSize.A4));
	}

}
