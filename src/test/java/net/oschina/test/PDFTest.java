package net.oschina.test;

import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfWriter;

public class PDFTest {

    public static final String RESULT = "test_file/test.pdf";
    /** Some text */
    public static final String TEXT
        = "你好，我叫杨英强。\nHi，my name is Cevin Yang. こんにちは、私の名前は杨英强";
	
	@Test
	public void test() throws IOException, DocumentException{
        Document document = new Document(PageSize.A4);
        PdfWriter.getInstance(document, new FileOutputStream(RESULT));
        document.open();
        
//        BaseFont bfChinese = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H",   
//                BaseFont.NOT_EMBEDDED);   
        BaseFont bfChinese = BaseFont.createFont("resource/font/MSYH.TTF", BaseFont.IDENTITY_H,BaseFont.NOT_EMBEDDED);
                  
        Font fontYHBold = new Font(bfChinese, 14, Font.BOLD);  
        Font fontYHNormal = new Font(bfChinese, 14, Font.NORMAL);  
        Font fontYHItaltc = new Font(bfChinese, 14, Font.ITALIC);
        Paragraph p = new Paragraph();
        p.add(new Chunk("中文, English，日本語の", fontYHBold));
        p.add(new Chunk("中文, English，日本語の", fontYHNormal));
        p.add(new Chunk("中文, English，日本語の", fontYHItaltc));

        
        document.add(p);
        document.add(new Paragraph(TEXT, fontYHNormal));

        document.close();
	}
}